/********************************************************************************************************
* Name: CalculateCaseAgeInBusinessDaysScheBatch
---------------------------------------------------------------------------------------------------------
* Purpose/Methods:/ 
* This is used as a schedule batch batchable, schedulable Apex Class to auto-update Case.
  Case_Age_in_Business_Days__c. Code should calculate number of company business days (not hours) 
  from Case's date opened to date closed (if Case is closed), or date opened to today (if Case is open)
********************************************************************************************************/

global with sharing class CalculateCaseAgeInBusinessDaysScheBatch implements Database.Batchable<sObject>,Schedulable{ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){   

        return Database.getQueryLocator([SELECT Id, CaseNumber, BusinessHoursId, Status, IsClosed, ClosedDate,
                                         CreatedDate, Case_Age_in_Business_Days__c FROM Case]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        //Get the default business hours
        BusinessHours defaultHours = [select Id from BusinessHours where IsDefault=true];
        
        //Default Business Hours Per Day
        String businessHoursPerDay = System.Label.Business_Hours_Per_Day;
        Integer defaultBusinessHoursPerDay;
        if(String.isNotBlank(businessHoursPerDay)){
            defaultBusinessHoursPerDay = Integer.valueOf(businessHoursPerDay);
        }
        
        List<Case> listCaseToUpdate = new List<Case>();
        for(Case updatedCase : (List<Case>)scope){
            
            //On the off-chance that the business hours on the case are null, use the default ones instead
            Id hoursToUse = updatedCase.BusinessHoursId != null ? updatedCase.BusinessHoursId : defaultHours.Id;
            
            DateTime dt;
            Date todaysDate = System.today();
            
            //Check if case is closed then use closedDate else todays DateTime
            if(updatedCase.IsClosed == true && updatedCase.ClosedDate != null){
                dt = updatedCase.ClosedDate;
            }else{
                //dt = DateTime.newInstance(todaysDate.year(), todaysDate.month(), todaysDate.day(), 0, 0, 0);
                dt = System.now();
            }
            
            //Returns the difference in milliseconds between a start and end Datetime based on a specific set of business hours.
            Double timeInMilliseconds = BusinessHours.diff(hoursToUse, updatedCase.CreatedDate , dt);
            
            //Calculate Business Days from milliseconds
            Decimal days = timeInMilliseconds/(defaultBusinessHoursPerDay * 60 * 60 * 1000);
            Decimal roundingUpOfDays;
            if(days != null){
                if(roundingUpOfDays < 0){
                    roundingUpOfDays = 0;
                }else{
                    roundingUpOfDays = days.round(System.RoundingMode.UP);
                } 
            }
            if(roundingUpOfDays != null){
                updatedCase.Case_Age_in_Business_Days__c = Integer.valueOf(roundingUpOfDays);
            }
            listCaseToUpdate.add(updatedCase);
        }
        if(listCaseToUpdate.size() > 0){
            Database.update(listCaseToUpdate, false);
        }
    }
    
    global void finish(Database.BatchableContext BC){
      
    }
 
    global void execute(SchedulableContext SC) {
         Database.executeBatch(new CalculateCaseAgeInBusinessDaysScheBatch(), 200);
    }
}