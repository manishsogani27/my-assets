/*String data = '003r000000PXZrQ'; //Passing contact Id randomly. 
String encodedCipherText = EncryptDecryptUtility.encryptData(data);
System.debug('==encodedCipherText==' + encodedCipherText);
String decryptedClearText = EncryptDecryptUtility.decryptData(encodedCipherText);
System.debug('==decryptedClearText==' + decryptedClearText);
*/
public class EncryptDecryptUtility {
    
    @AuraEnabled
    public static String encryptData(String data){
        try {
            Blob key = Blob.valueOf('S6aDz0Yy3R7iTRDuRBs0tfrdfkwbhtda');
            Blob cipherText = Crypto.encryptWithManagedIV('AES256', key, Blob.valueOf(data));
            String encodedCipherText = EncodingUtil.base64Encode(cipherText); 
            return encodedCipherText;
        }catch (Exception e) { 
            throw new UCException( e.getMessage() ); 
        }
    }
    
    @AuraEnabled
    public static String decryptData(String data){
        try {
            Blob key = Blob.valueOf('S6aDz0Yy3R7iTRDuRBs0tfrdfkwbhtda');
            Blob encodedEncryptedBlob = EncodingUtil.base64Decode(data);
            Blob decryptedBlob = Crypto.decryptWithManagedIV('AES256', key, encodedEncryptedBlob);
            String decryptedClearText = decryptedBlob.toString();
            return decryptedClearText;
        }catch (Exception e) { 
            throw new UCException( e.getMessage() );
        }
    }
    
    public class UCException extends Exception{
        
    }
    
}