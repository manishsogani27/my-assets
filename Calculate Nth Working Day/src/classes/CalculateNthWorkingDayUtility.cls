//Integer N = 1;
//CalculateNthWorkingDayUtility.calculateNthWorkingDay(System.today(),N);

public class CalculateNthWorkingDayUtility {
    
    //to calculate Nth business day 
    public static Date calculateNthWorkingDay(Date fromDate, Integer N){
        
        //Query hoildays from Org  
        List<Holiday> holidays=[Select StartTimeInMinutes, Name, ActivityDate From Holiday ];
        
        Date NthDay = fromDate;
        
        for(Integer k=0;k<N ;k++ ){
            if(checkifWorkDay(NthDay.addDays(1),holidays)){                        
                NthDay = NthDay.addDays(1);
            } 
            else{
                NthDay = NthDay.addDays(1);
                K--;
            }
        }
        
        return NthDay;//send Nth Business day
    }
    
    //To check if sent date is a working day by comparing with org holidays
    public static boolean checkifWorkDay(Date sentDate,List<Holiday> holidays){
        
        Date weekStart  = sentDate.toStartofWeek();
        
        for(Holiday hday:holidays){
            if(sentDate.daysBetween(hday.ActivityDate) == 0)                        
                return false;                        
        }
        if(weekStart.daysBetween(sentDate) ==0 || weekStart.daysBetween(sentDate) == 6){
            return false;
        } else{
            return true;
        }
    }
    
}