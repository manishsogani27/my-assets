@isTest
private class CalcuCaseAgeInBusinessDaysScheBatchTest {
	
    static testMethod void myUnitTest1() {
        
        Case testCase = new Case();
        testCase.Origin = 'Email';
        testCase.Type = 'Complaint';
        insert testCase;
        
        try{	
			testCase.Status = 'Closed';
			update testCase;
		}catch(Exception ex){}
        
        Test.startTest();
            CalculateCaseAgeInBusinessDaysScheBatch batchScheduler = new CalculateCaseAgeInBusinessDaysScheBatch();
            String sch = '0 0 23 * * ?';
            system.schedule('Calculate Case Age In Business Days',sch , batchScheduler);
        Test.stopTest();    
    }
    
	static testMethod void myUnitTest2() {
        
        Case testCase = new Case();
        testCase.Origin = 'Email';
        testCase.Type = 'Complaint';
        insert testCase;
        
        Test.startTest();
            CalculateCaseAgeInBusinessDaysScheBatch batchScheduler = new CalculateCaseAgeInBusinessDaysScheBatch();
			database.executeBatch(batchScheduler, 10);
        Test.stopTest();    
    }
    
    
}