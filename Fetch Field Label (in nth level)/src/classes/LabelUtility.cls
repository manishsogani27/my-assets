/*LabelUtility cls = new LabelUtility();
System.debug('==FIELD LABEL==>' + cls.fetchFieldLabel('Contact', 'Contact.Account.Contact.Account.Owner.CommunityNickname'));
*/
public class LabelUtility {
	
    public String fetchFieldLabel(String objectName, String fieldName){
        //System.debug('==objectName==' + objectName);
        String fieldLabel = '';
        if(objectName != null){
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType obSchema = schemaMap.get(objectName);
            
            if(obSchema != null){
                Map<String, Schema.SObjectField> fieldMap = obSchema.getDescribe().fields.getMap();
                
                for (String s : fieldMap.keySet()) {
                    if(fieldName == s){
                        fieldLabel = fieldMap.get(fieldName).getDescribe().getLabel();
                        return fieldLabel;
                    }
                }
            }
            List<String> lstString = new List<String>();
            if(fieldLabel == '' || fieldLabel == null){
                if(fieldName != null && fieldName.indexOf('.') != -1){
                    fieldName = fieldName.replace('.',',');
                    for(String fs : fieldName.split(',')){
                        lstString.add(fs);
                    }
                    if(lstString.size() > 0){
                        Integer j = 0;
                        String relName = '';
                        String rFName = '';
                        String mainObject = objectName;
                        for(Integer i = 0; i < lstString.size(); i++){
                            relName = lstString.get(j);
                            for(Integer k = 0; k < lstString.size(); k++){
                                if(j < k){
                                    rFName += lstString.get(k)+'.';
                                }
                            }
                            if(rFName != null && rFName!= ''){
                                rFName = rFName.substring(0, rFName.lastIndexOf('.'));
                            }
                            //System.debug('==relName==' + relName);
                            //System.debug('==rFName==' + rFName);
                            if(relName != '' && relName != null && rFName != '' && rFName != null){
                                ObjNameWrapper wra = fetchLabel(mainObject, relName, rFName, schemaMap, obSchema);
                                fieldLabel = wra.flsLabel;
                                if(fieldLabel == '' || fieldLabel == null){
                                    relName = '';
                                    rFName = '';
                                    mainObject = wra.poName;
                                }else{
                                    return fieldLabel;
                                }
                            }
                            j++;
                        }
                    }
                }
            }
        }
        return fieldLabel;
    }
    
    public ObjNameWrapper fetchLabel(String parentObjName, String relName, String restFieldsStr, Map<String, Schema.SObjectType> schemaMap, Schema.SObjectType obSchema){
        //System.debug('=i=parentObjName==' + parentObjName);
        String newParentObjName = '';
        if(parentObjName != null && String.isNotBlank(parentObjName)){
            
            for(Schema.SobjectField strFld: obSchema.getDescribe().fields.getMap().Values()){
                if(strFld.getDescribe().getType() == Schema.DisplayType.REFERENCE){
                    if(strFld.getDescribe().getRelationshipName() == relName){
                        newParentObjName = strFld.getDescribe().getReferenceTo()+'';
                    }
                } 
            }
        }
        if(newParentObjName != null && newParentObjName != '' && newParentObjName.indexOf('(') != -1){
            newParentObjName = newParentObjName.replace('(',' ');
            newParentObjName = newParentObjName.trim();
        }
        if(newParentObjName != null && newParentObjName != '' && newParentObjName.indexOf(')') != -1){
            newParentObjName = newParentObjName.replace(')',' ');
            newParentObjName = newParentObjName.trim();
        }
        if(newParentObjName == ''){
            newParentObjName = parentObjName;
        }
        //System.debug('=r=newParentObjName==' + newParentObjName);
        return new ObjNameWrapper(newParentObjName, fetchFieldLabel(newParentObjName, restFieldsStr));
    }
    
    public class ObjNameWrapper{
        public String poName {get;set;}
        public String flsLabel {get;set;}
        public ObjNameWrapper(String poName, String flsLabel){
            this.poName = poName;
            this.flsLabel = flsLabel;
        }
    }
    
}